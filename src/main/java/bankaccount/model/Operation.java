package bankaccount.model;

import bankaccount.exception.InvalidOperationAmountException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Objects;

public class Operation {

    public static Operation of(final long accountId, final Type type, final LocalDateTime time, final BigDecimal amount, final BigDecimal newBalance) throws InvalidOperationAmountException {
        final var scaledAmount = amount.setScale(2, RoundingMode.HALF_UP);
        final var scaledNewBalance = newBalance.setScale(2, RoundingMode.HALF_UP);

        if (scaledAmount.compareTo(BigDecimal.ZERO) < 0)
            throw new InvalidOperationAmountException(scaledAmount);

        return new Operation(accountId, type, time, scaledAmount, scaledNewBalance);
    }

    private final long accountId;
    private final Type type;
    private final LocalDateTime time;
    private final BigDecimal amount;
    private final BigDecimal newBalance;

    private Operation(final long accountId, final Type type, final LocalDateTime time, final BigDecimal amount, final BigDecimal newBalance) {
        this.accountId = accountId;
        this.type = type;
        this.time = time;
        this.amount = amount;
        this.newBalance = newBalance;
    }

    public long accountId() {
        return accountId;
    }

    public Type type() {
        return type;
    }

    public LocalDateTime time() {
        return time;
    }

    public BigDecimal amount() {
        return amount;
    }

    public BigDecimal newBalance() {
        return newBalance;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        final var that = (Operation) obj;
        return this.accountId == that.accountId &&
                Objects.equals(this.type, that.type) &&
                Objects.equals(this.time, that.time) &&
                Objects.equals(this.amount, that.amount) &&
                Objects.equals(this.newBalance, that.newBalance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, type, time, amount, newBalance);
    }

    @Override
    public String toString() {
        return "Operation[" +
                "accountId=" + accountId + ", " +
                "type=" + type + ", " +
                "time=" + time + ", " +
                "amount=" + amount + ", " +
                "newBalance=" + newBalance + ']';
    }

    public enum Type {
        DEPOSIT,
        WITHDRAW,
    }
}
