package bankaccount.service;

import bankaccount.model.Operation;

import java.util.List;

public interface OperationFormatter {
    List<String> format(List<Operation> operationList);
}