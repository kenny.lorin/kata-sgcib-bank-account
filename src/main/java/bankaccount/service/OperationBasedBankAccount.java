package bankaccount.service;

import bankaccount.exception.InsufficientFundsException;
import bankaccount.exception.InvalidOperationAmountException;
import bankaccount.model.Operation;
import bankaccount.repository.OperationsRepository;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;

import static bankaccount.model.Operation.Type.DEPOSIT;
import static bankaccount.model.Operation.Type.WITHDRAW;

public record OperationBasedBankAccount(OperationsRepository operationsRepository, Clock clock) implements BankAccount {

    @Override
    public void deposit(final long accountId, final BigDecimal amount) throws InvalidOperationAmountException {
        final var newBalance = operationsRepository
                .findLast(accountId)
                .map(Operation::newBalance)
                .orElse(BigDecimal.ZERO)
                .add(amount);

        final var operation = Operation.of(
                accountId,
                DEPOSIT,
                LocalDateTime.now(clock),
                amount,
                newBalance
        );

        operationsRepository.save(operation);
    }

    @Override
    public void withdraw(final long accountId, final BigDecimal amount) throws InvalidOperationAmountException, InsufficientFundsException {
        final var oldBalance = operationsRepository.findLast(accountId)
                .map(Operation::newBalance)
                .orElse(BigDecimal.ZERO);

        if (oldBalance.compareTo(amount) < 0)
            throw new InsufficientFundsException(oldBalance, amount);

        final var newBalance = oldBalance.subtract(amount);

        final var operation = Operation.of(
                accountId,
                WITHDRAW,
                LocalDateTime.now(clock),
                amount,
                newBalance
        );

        operationsRepository.save(operation);
    }

    @Override
    public void printStatement(final long accountId, final OperationFormatter formatter, final StatementPrinter printer) {
        final var operations = formatter.format(operationsRepository.findAllByAccountId(accountId));

        printer.print(operations);
    }
}
