package bankaccount.service;

import bankaccount.exception.AccountNotFoundException;
import bankaccount.exception.InsufficientFundsException;
import bankaccount.exception.InvalidOperationAmountException;

import java.math.BigDecimal;

public interface BankAccount {
    void deposit(long accountId, BigDecimal amount)
            throws AccountNotFoundException, InvalidOperationAmountException;

    void withdraw(long accountId, BigDecimal amount)
            throws AccountNotFoundException, InvalidOperationAmountException, InsufficientFundsException;

    void printStatement(long accountId, OperationFormatter formatter, StatementPrinter printer)
            throws AccountNotFoundException;
}
