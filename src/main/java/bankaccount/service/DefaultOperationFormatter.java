package bankaccount.service;

import bankaccount.model.Operation;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class DefaultOperationFormatter implements OperationFormatter {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final Function<Operation, String> operationFormatter = operation -> String.format(
            "| %s | %8s | %23.2f |",
            operation.time().format(formatter),
            operation.type().name(),
            operation.amount()
    );

    @Override
    public List<String> format(final List<Operation> operations) {
        final var header = "|    Date and Time    |   Type   |         Amount          |";
        final var delim = "|---------------------|----------|-------------------------|";
        final var footer =
                          "| Balance: %47.2f |".formatted(
                operations.stream()
                        .reduce((first, second) -> second)
                        .map(Operation::newBalance)
                        .orElse(BigDecimal.ZERO)
        );

        final var headerWithDelim = Stream.of(delim, header, delim);

        final var formattedOperations = operations.stream()
                .map(operationFormatter);

        final var footerWithDelim = Stream.of(delim, footer, delim);

        return Stream.of(headerWithDelim, formattedOperations, footerWithDelim)
                .flatMap(s -> s)
                .toList();
    }
}
