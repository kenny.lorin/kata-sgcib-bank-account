package bankaccount.service;

import java.util.List;

public interface StatementPrinter {
    void print(List<String> lines);
}
