package bankaccount.service;

import java.io.PrintStream;
import java.util.List;

public class PrintStreamStatementPrinter implements StatementPrinter {
    private final PrintStream stream;

    public PrintStreamStatementPrinter(final PrintStream stream) {
        this.stream = stream;
    }

    @Override
    public void print(final List<String> lines) {
        lines.forEach(stream::println);
    }
}
