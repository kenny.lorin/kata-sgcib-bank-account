package bankaccount.repository;

import bankaccount.model.Operation;

import java.util.List;
import java.util.Optional;

public interface OperationsRepository {
    void save(Operation operation);

    List<Operation> findAllByAccountId(long accountId);

    Optional<Operation> findLast(long accountId);
}
