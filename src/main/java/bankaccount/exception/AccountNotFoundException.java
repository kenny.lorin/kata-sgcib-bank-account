package bankaccount.exception;

public class AccountNotFoundException extends Exception {
    public AccountNotFoundException(final long accountId) {
        super("Account of id %d does not exist".formatted(accountId));
    }
}
