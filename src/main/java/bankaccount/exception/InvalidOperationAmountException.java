package bankaccount.exception;

import java.math.BigDecimal;

public class InvalidOperationAmountException extends Exception {
    public InvalidOperationAmountException(final BigDecimal amount) {
        super("Invalid operation amount: %.2f".formatted(amount));
    }
}
