package bankaccount.exception;

import java.math.BigDecimal;

public class InsufficientFundsException extends Exception {
    public InsufficientFundsException(final BigDecimal balance, final BigDecimal amount) {
        super("Insufficient balance of %.2f for operation of amount %.2f".formatted(balance, amount));
    }
}
