package bankaccount;

import bankaccount.exception.InsufficientFundsException;
import bankaccount.exception.InvalidOperationAmountException;
import bankaccount.model.Operation;
import bankaccount.repository.OperationsRepository;
import bankaccount.service.DefaultOperationFormatter;
import bankaccount.service.OperationBasedBankAccount;
import bankaccount.service.PrintStreamStatementPrinter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountKataTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    static class InMemoryOperationsRepository implements OperationsRepository {
        final LinkedList<Operation> operations = new LinkedList<>();

        @Override
        public void save(final Operation operation) {
            operations.add(operation);
        }

        @Override
        public List<Operation> findAllByAccountId(final long accountId) {
            return operations;
        }

        @Override
        public Optional<Operation> findLast(final long accountId) {
            if (operations.isEmpty())
                return Optional.empty();
            return Optional.of(operations.getLast());
        }
    }

    @Test
    @DisplayName("should follow the correct behavior")
    void integrationTest() throws InvalidOperationAmountException, InsufficientFundsException {
        // Arrange
        final var operationRepository = new InMemoryOperationsRepository();
        final var clock = Clock.fixed(Instant.parse("2042-10-11T19:32:42.00Z"), ZoneId.of("Europe/Paris"));
        final var bankAccount = new OperationBasedBankAccount(operationRepository, clock);
        final var formatter = new DefaultOperationFormatter();
        final var printer = new PrintStreamStatementPrinter(new PrintStream(byteArrayOutputStream));

        final var accountId = 0xace;
        final var eol = System.lineSeparator();
        final var expected = "|---------------------|----------|-------------------------|" + eol
                + "|    Date and Time    |   Type   |         Amount          |" + eol
                + "|---------------------|----------|-------------------------|" + eol
                + "| 2042-10-11 21:32:42 |  DEPOSIT |                   42.00 |" + eol
                + "| 2042-10-11 21:32:42 |  DEPOSIT |                   44.00 |" + eol
                + "| 2042-10-11 21:32:42 | WITHDRAW |                   86.00 |" + eol
                + "| 2042-10-11 21:32:42 |  DEPOSIT |                  101.00 |" + eol
                + "|---------------------|----------|-------------------------|" + eol
                + "| Balance:                                          101.00 |" + eol
                + "|---------------------|----------|-------------------------|" + eol;

        // Act
        bankAccount.deposit(accountId, new BigDecimal("42"));
        bankAccount.deposit(accountId, new BigDecimal("44"));
        bankAccount.withdraw(accountId, new BigDecimal("86"));
        bankAccount.deposit(accountId, new BigDecimal("101"));
        bankAccount.printStatement(accountId, formatter, printer);
        bankAccount.withdraw(accountId, new BigDecimal("51"));

        // Assert
        final var actual = byteArrayOutputStream.toString();
        assertEquals(expected, actual);
    }
}
