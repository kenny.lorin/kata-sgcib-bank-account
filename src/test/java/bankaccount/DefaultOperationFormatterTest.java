package bankaccount;

import bankaccount.exception.InvalidOperationAmountException;
import bankaccount.model.Operation;
import bankaccount.service.DefaultOperationFormatter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static bankaccount.model.Operation.Type.DEPOSIT;
import static bankaccount.model.Operation.Type.WITHDRAW;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("DefaultOperationFormatter")
class DefaultOperationFormatterTest {

    final Clock clock = Clock.fixed(Instant.parse("2042-10-11T19:32:42.00Z"), ZoneId.of("Europe/Paris"));
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final DefaultOperationFormatter operationFormatter = new DefaultOperationFormatter();

    @Test
    @DisplayName("should format the given operations")
    void shouldFormatTheGivenOperations() throws InvalidOperationAmountException {
        // Arrange
        final var operation = List.of(
                Operation.of(
                        0xd00f,
                        DEPOSIT,
                        LocalDateTime.now(clock),
                        new BigDecimal("42.1234"),
                        new BigDecimal("51.4269")
                ),
                Operation.of(
                        0xbed,
                        WITHDRAW,
                        LocalDateTime.now(clock),
                        new BigDecimal("1.234"),
                        new BigDecimal("42.69")
                )
        );

        final var expected = List.of(
                "|---------------------|----------|-------------------------|",
                "|    Date and Time    |   Type   |         Amount          |",
                "|---------------------|----------|-------------------------|",
                "| 2042-10-11 21:32:42 |  DEPOSIT |                   42.12 |",
                "| 2042-10-11 21:32:42 | WITHDRAW |                    1.23 |",
                "|---------------------|----------|-------------------------|",
                "| Balance:                                           42.69 |",
                "|---------------------|----------|-------------------------|"
        );

        // Act
        final var actual = operationFormatter.format(operation);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("should return an empty list given an empty list")
    void shouldReturnAnEmptyListGivenAnEmptyList() {
        // Arrange
        final var operations = List.<Operation>of();
        final var expected = List.of(
                "|---------------------|----------|-------------------------|",
                "|    Date and Time    |   Type   |         Amount          |",
                "|---------------------|----------|-------------------------|",
                "|---------------------|----------|-------------------------|",
                "| Balance:                                            0.00 |",
                "|---------------------|----------|-------------------------|"
        );

        // Act
        final var actual = operationFormatter.format(operations);

        // Assert
        assertEquals(expected, actual);
    }
}