package bankaccount;

import bankaccount.exception.AccountNotFoundException;
import bankaccount.exception.InsufficientFundsException;
import bankaccount.exception.InvalidOperationAmountException;
import bankaccount.model.Operation;
import bankaccount.repository.OperationsRepository;
import bankaccount.service.OperationBasedBankAccount;
import bankaccount.service.OperationFormatter;
import bankaccount.service.PrintStreamStatementPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static bankaccount.model.Operation.Type.DEPOSIT;
import static bankaccount.model.Operation.Type.WITHDRAW;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("DefaultBankAccount")
class OperationBasedBankAccountTest {

    final Clock clock = Clock.fixed(Instant.parse("2042-10-11T19:32:42.00Z"), ZoneId.of("Europe/Paris"));

    @Mock
    private OperationsRepository operationsRepository;

    @Mock
    private OperationFormatter operationFormatter;

    @Mock
    private PrintStreamStatementPrinter printStreamOperationsPrinter;

    private OperationBasedBankAccount bankAccount;

    @BeforeEach
    void setup() {
        bankAccount = new OperationBasedBankAccount(operationsRepository, clock);
    }

    @Test
    @DisplayName("deposit should throw given a negative amount")
    void depositShouldThrowGivenNegativeAmount() {
        // Arrange
        final var accountId = 0xdeadbeef;
        final var amount = new BigDecimal("-420.69");
        final var expected = "Invalid operation amount: -420.69";

        // Act
        final var exception = assertThrows(
                InvalidOperationAmountException.class,
                () -> bankAccount.deposit(accountId, amount)
        );

        // Assert
        assertEquals(expected, exception.getMessage());
        verify(operationsRepository).findLast(accountId);
        verifyNoMoreInteractions(operationsRepository);
    }

    @Test
    @DisplayName("deposit should save an operation with new balance given valid amount")
    void depositShouldSaveAnOperationWithNewBalanceGivenAValidAmount() throws InvalidOperationAmountException {
        // Arrange
        final var accountId = 0xdeadbeef;
        final var amount = new BigDecimal("51").setScale(2, RoundingMode.HALF_EVEN);
        final var initialBalance = new BigDecimal("18").setScale(2, RoundingMode.HALF_EVEN);
        final var expected = Operation.of(
                accountId,
                DEPOSIT,
                LocalDateTime.now(clock),
                amount,
                initialBalance.add(amount)
        );

        when(operationsRepository.findLast(accountId)).thenReturn(Optional.of(Operation.of(
                accountId,
                DEPOSIT,
                LocalDateTime.now(clock),
                initialBalance,
                initialBalance
        )));

        // Act
        bankAccount.deposit(accountId, amount);

        // Assert
        final var inOrderRepository = inOrder(operationsRepository);
        inOrderRepository.verify(operationsRepository).findLast(accountId);
        inOrderRepository.verify(operationsRepository).save(expected);
        inOrderRepository.verifyNoMoreInteractions();
    }

    @Test
    @DisplayName("withdraw should throw given a negative amount")
    void withdrawShouldThrowGivenNegativeAmount() {
        // Arrange
        final var accountId = 0xcafebabe;
        final var amount = new BigDecimal("-420.69").setScale(2, RoundingMode.HALF_EVEN);
        final var expected = "Invalid operation amount: -420.69";

        // Act
        final var exception = assertThrows(
                InvalidOperationAmountException.class,
                () -> bankAccount.withdraw(accountId, amount)
        );

        // Assert
        assertEquals(expected, exception.getMessage());
        verify(operationsRepository).findLast(accountId);
        verifyNoMoreInteractions(operationsRepository);
    }

    @Test
    @DisplayName("withdraw should throw given an amount greater than the balance of the account")
    void withdrawShouldThrowGivenAnAmountGreaterThanBalanceOfAccount() throws InvalidOperationAmountException {
        // Arrange
        final var accountId = 0xcafebabe;
        final var amount = new BigDecimal("42.44").setScale(2, RoundingMode.HALF_EVEN);
        final var initialBalance = new BigDecimal("18").setScale(2, RoundingMode.HALF_EVEN);

        final var expected = "Insufficient balance of 18.00 for operation of amount 42.44";

        when(operationsRepository.findLast(accountId)).thenReturn(Optional.of(Operation.of(
                accountId,
                DEPOSIT,
                LocalDateTime.now(clock),
                initialBalance,
                initialBalance
        )));

        // Act
        final var exception = assertThrows(
                InsufficientFundsException.class,
                () -> bankAccount.withdraw(accountId, amount)
        );

        // Assert
        assertEquals(expected, exception.getMessage());

        verify(operationsRepository).findLast(accountId);
        verifyNoMoreInteractions(operationsRepository);
    }

    @Test
    @DisplayName("withdraw should return an operation with new balance given valid amount")
    void withdrawShouldReturnAnOperationWithNewBalanceGivenAValidAmount() throws InvalidOperationAmountException, InsufficientFundsException {
        // Arrange
        final var accountId = 0xcafebabe;
        final var amount = new BigDecimal("50").setScale(2, RoundingMode.HALF_EVEN);
        final var initialBalance = new BigDecimal("2048").setScale(2, RoundingMode.HALF_EVEN);
        final var expected = Operation.of(
                accountId,
                WITHDRAW,
                LocalDateTime.now(clock),
                amount,
                initialBalance.subtract(amount)
        );

        when(operationsRepository.findLast(accountId)).thenReturn(Optional.of(Operation.of(
                accountId,
                DEPOSIT,
                LocalDateTime.now(clock),
                initialBalance,
                initialBalance
        )));

        // Act
        bankAccount.withdraw(accountId, amount);

        // Assert
        final var inOrderRepository = inOrder(operationsRepository);
        inOrderRepository.verify(operationsRepository).findLast(accountId);
        inOrderRepository.verify(operationsRepository).save(expected);
        inOrderRepository.verifyNoMoreInteractions();
    }

    @Test
    @DisplayName("printStatement should print the operations of the account of given id")
    void printStatementShouldPrintTheOperationsOfTheAccountOfGivenId() throws InvalidOperationAmountException {
        // Arrange
        final var accountId = 0xf00d;
        final var operations = List.of(
                Operation.of(accountId, DEPOSIT, LocalDateTime.now(clock), new BigDecimal("51").setScale(2, RoundingMode.HALF_EVEN), new BigDecimal("51").setScale(2, RoundingMode.HALF_EVEN)),
                Operation.of(accountId, WITHDRAW, LocalDateTime.now(clock), new BigDecimal("9").setScale(2, RoundingMode.HALF_EVEN), new BigDecimal("42").setScale(2, RoundingMode.HALF_EVEN))
        );
        final var formattedOperations = List.of("formatted_deposit", "formatted_withdraw");

        when(operationsRepository.findAllByAccountId(accountId)).thenReturn(operations);
        when(operationFormatter.format(operations)).thenReturn(formattedOperations);

        // Act
        bankAccount.printStatement(accountId, operationFormatter, printStreamOperationsPrinter);

        // Assert
        final var inOrderSteps = inOrder(operationsRepository, operationFormatter, printStreamOperationsPrinter);
        inOrderSteps.verify(operationsRepository).findAllByAccountId(accountId);
        inOrderSteps.verify(operationFormatter).format(operations);
        inOrderSteps.verify(printStreamOperationsPrinter).print(formattedOperations);
        inOrderSteps.verifyNoMoreInteractions();
    }
}