package bankaccount;

import bankaccount.service.PrintStreamStatementPrinter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.PrintStream;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("PrintStreamStatementPrinter")
class PrintStreamStatementPrinterTest {

    @Mock
    private PrintStream printStream;

    @InjectMocks
    private PrintStreamStatementPrinter printStreamOperationsPrinter;

    @Test
    @DisplayName("should print the given non-empty list to the stream")
    void shouldPrintTheGivenNonEmptyListToTheStream() {
        // Arrange
        final var strings = List.of("Hello", "world!");

        // Act
        printStreamOperationsPrinter.print(strings);

        // Assert
        final var inOrderPrint = inOrder(printStream);
        inOrderPrint.verify(printStream).println("Hello");
        inOrderPrint.verify(printStream).println("world!");
        inOrderPrint.verifyNoMoreInteractions();
    }

    @Test
    @DisplayName("should print the given empty list to the stream")
    void shouldPrintTheGivenEmptyListToTheStream() {
        // Arrange
        final var strings = List.<String>of();

        // Act
        printStreamOperationsPrinter.print(strings);

        // Assert
        verifyNoMoreInteractions(printStream);
    }
}